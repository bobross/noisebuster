# Noise Buster #
This repository contains sources for the Noise Buster application ecosystem.

# Structure #
The different sub-folders contains:
* Xamarin: The Cross-Platform Configuration application for mobile devices
* cloud: ...

## Rest Web-Services ##
Azure:

* Hostname: https://zeatmobileservice.azure-mobile.net
* Application key for Azure Cloud: HTTP header key "X-ZUMO-APPLICATION" with key "lUeMJPkuUdJYCqRrBgTZMIPrbKmhcM86"

Heroku: 

* Hostname: http://noisebuster-sails-rest.herokuapp.com

