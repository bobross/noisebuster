(function () {
    'use strict';

    angular
        .module('noisebuster', ['ngRoute', 'ngMaterial', 'noisebuster.sensors']);

})();
