(function () {
    'use strict';

    angular.module('noisebuster.service').factory("sensorService", function ($resource, config) {
        return $resource(config.backend + "/sensor/:id", null,
            {
                get: {
                    method: 'GET',
                    headers: {
                        'X-ZUMO-APPLICATION': config.headerValue
                    }
                },

                'update': {
                    method: 'PUT',
                    'params': {
                        'id': "@id"
                    },
                    headers: {
                        'X-ZUMO-APPLICATION': config.headerValue
                    }
                },

                'delete': {
                    method: 'DELETE',
                    'params': {
                        'id': "@id"
                    },
                    headers: {
                        'X-ZUMO-APPLICATION': config.headerValue
                    }
                },
            });
    });

})();