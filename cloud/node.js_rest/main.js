//var http = require('http');
//http.createServer(function handler(req, res) {
//    res.writeHead(200, {'Content-Type': 'text/plain'});
//    res.end('Hello World\n');
//}).listen(1337, '127.0.0.1');
//console.log('Server running at http://127.0.0.1:1337/');


var restify = require('restify')
, fs = require('fs');


var controllers = {}
, controllers_path = process.cwd() + '/controller';
fs.readdirSync(controllers_path).forEach(function (file) {
if (file.indexOf('.js') != -1) {
    controllers[file.split('.')[0]] = require(controllers_path + '/' + file);
}
});

var server = restify.createServer();

server
.use(restify.fullResponse())
.use(restify.bodyParser());

//Article Start
server.post("/sensor", controllers.sensor.createSensor);
server.put("/sensor/:id", controllers.sensor.updateSensor);
server.get("/sensor", controllers.sensor.viewAllSensors);
server.get("/sensor/:id", controllers.sensor.viewSensor);
server.del("/sensor/:id", controllers.sensor.deleteSensor);
//Article End

//Comment Start
//server.get({path: "/articles/:id", version: "1.0.0"}, controllers.article.viewArticle)
//server.get({path: "/articles/:id", version: "2.0.0"}, controllers.article.viewArticle_v2)
//Comment End

var port = process.env.PORT || 3000;
server.listen(port, function (err) {
	if (err){
	    console.error(err);
	}
	else {
	    console.log('App is ready at : ' + port);
	}
});

if (process.env.environment == 'production'){
	process.on('uncaughtException', function (err) {
	    console.error(JSON.parse(JSON.stringify(err, ['stack', 'message', 'inner'], 2)));
	});
}