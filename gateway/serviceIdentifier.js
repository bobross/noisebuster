function ServiceIdentifier(peripheralUuid, serviceUuid) {
  this.peripheralUuid = peripheralUuid;
  this.serviceUuid = serviceUuid;
}

ServiceIdentifier.prototype.equals = function (other) {
  if (typeof this !== typeof other)
  {
    return false;
  }

if (this === other) {
    return true;
  }
  
}