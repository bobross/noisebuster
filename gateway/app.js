var REMOTE_HOST = 'zeatmobileservice.azure-mobile.net';
var APPLICATION_KEY = "lUeMJPkuUdJYCqRrBgTZMIPrbKmhcM86";

var SINGLE_SENSOR_PATH = '/api/sensor/{id}';
var SENSOR_MEASUREMENTS_PATH = '/api/sensor/{id}/measurement/';
var SINGLE_ACTOR_PATH = '/api/actor/{id}';
var ACTOR_PATH = '/api/actor';

var SUPPORTED_MANUFACTURER_DATA = 'affe5a7565686c6b652043616d702032303135';

var LOUDNESS_SENSOR_SERVICE_UUID = 'bee0';
var LOUDNESS_SENSOR_TYPE = 'Loudness';
var LOUDNESS_CHARACTERISTIC_UUID = 'bee1';

var AMBIENT_LIGHT_SENSOR_SERVICE_UUID = 'cafe';
var AMBIENT_LIGHT_SENSOR_TYPE = 'AmbientLight';
var AMBIENT_LIGHT_CHARACTERISTIC_UUID = 'caff';

var ALARM_LIGHT_ACTOR_SERVICE_UUID = 'dead';
var ALARM_LIGHT_ACTOR_TYPE = 'AlarmLight';
var ALARM_LIGHT_CHARACTERISTIC_UUID = 'dea1';

var SUPPORTED_SERVICE_UUIDS = [LOUDNESS_SENSOR_SERVICE_UUID, AMBIENT_LIGHT_SENSOR_SERVICE_UUID, ALARM_LIGHT_ACTOR_SERVICE_UUID];

var async = require('async');
var noble = require('noble');
var colors = require('colors');
var https = require('https');
var util = require('util');

var initializedServices = {};
var sensorCharacteristics = {};
var alarmCharacteristics = {};

setInterval(synchronize, 1000);

function synchronize() {
  readSensors();
  updateAlarms();
}

function readSensors() {
  for (id in sensorCharacteristics) {
    sensorCharacteristics[id].read();    
  }
}

function updateAlarms() {
  var path = ACTOR_PATH + '?withAlarm=true';

  sendRequest(path, null, 'GET', function(error, response) {
    if (error) {
      console.log('An error occurred while trying to get the alarmed actors: ' + error.toString());
    } else {
      // TODO
    }
  });
}

noble.on('stateChange', function (state) {
  if (state === 'poweredOn') {
    noble.startScanning();
  } else {
    noble.stopScanning();
  }
});

noble.on('discover', function peripheralDiscovered(peripheral) {
  var peripheralUuid = peripheral.uuid;
  console.log('Peripheral with UUID ' + peripheralUuid + ' found:');
  
  var advertisement = peripheral.advertisement;
  var localName = advertisement.localName;
  var manufacturerData = advertisement.manufacturerData;
  var serviceData = advertisement.serviceData;
  var serviceUuids = advertisement.serviceUuids;
  
  if (localName) {
    console.log('  Local Name    = ' + localName);
  }
  
  if (manufacturerData) {
    console.log('  Manufacturer Data = ' + manufacturerData.toString('hex'));
  }
  
  if (serviceData) {
    console.log('  Service Data    = ' + serviceData);
  }
  
  if (localName) {
    console.log('  Service UUIDs   = ' + serviceUuids);
  }
  
  if (isSupportedPeripherial(peripheral)) {
    console.log('Identified ' + localName + ' as supported peripheral, connecting...');
    
    peripheral.on('disconnect', function () {
      peripheralDisonnected(peripheral);
    });
    
    peripheral.connect(function (error) {
      peripheralConnected(error, peripheral);
    });
  } else {
    console.log('Peripheral ' + localName + ' is not supported.');
  }
});

function isSupportedPeripherial(peripheral) {
  return peripheral.advertisement.manufacturerData && 
    (peripheral.advertisement.manufacturerData.toString('hex') === SUPPORTED_MANUFACTURER_DATA);
}

function peripheralConnected(error, peripheral) {
  console.log('Connected, looking for services...');
  
  peripheral.discoverServices(SUPPORTED_SERVICE_UUIDS, function (error, services) {
    peripheralServicesDiscovered(error, services, peripheral);
  });
}

function peripheralDisonnected(peripheral) {
  // TODO
}

function peripheralServicesDiscovered(error, services, peripheral) {
  services.forEach(function (service) {
    initializeService(service, peripheral);
  });
}

function initializeService(service, peripheral) {
  var serviceUuid = service.uuid;
  var globalServiceIdentifier = getGlobalServiceIdentifier(peripheral, serviceUuid);
  
  if (isAlreadyInitialized(globalServiceIdentifier)) {
    return;
  }
  
  console.log('Initializing service ' + globalServiceIdentifier + '...');
  
  if (serviceUuid === LOUDNESS_SENSOR_SERVICE_UUID) {
    initializeLoudnessSensor(service, globalServiceIdentifier);
  } else if (serviceUuid === AMBIENT_LIGHT_SENSOR_SERVICE_UUID) {
    initializeAmbientLightSensor(service, globalServiceIdentifier);
  } else if (serviceUuid === ALARM_LIGHT_ACTOR_SERVICE_UUID) {
    initializeAlarmLightActor(service, globalServiceIdentifier);
  }
  
  setAsInitialized(globalServiceIdentifier);
}

function getGlobalServiceIdentifier(peripheral, service) {
  return peripheral.uuid.toString() + ':' + service.toString();
}

function isAlreadyInitialized(globalServiceIdentifier) {
  return globalServiceIdentifier in initializedServices;
}

function setAsInitialized(globalServiceIdentifier) {
  initializedServices[globalServiceIdentifier] = true;
}

function initializeLoudnessSensor(service, globalServiceIdentifier) {
  registerSensor(globalServiceIdentifier, LOUDNESS_SENSOR_TYPE);
  setupCharacteristicForSensorReading(service, LOUDNESS_CHARACTERISTIC_UUID, globalServiceIdentifier);
}

function initializeAmbientLightSensor(service, globalServiceIdentifier) {
  registerSensor(globalServiceIdentifier, AMBIENT_LIGHT_SENSOR_TYPE);
  setupCharacteristicForSensorReading(service, AMBIENT_LIGHT_CHARACTERISTIC_UUID, globalServiceIdentifier);
}

function initializeAlarmLightActor(service, globalServiceIdentifier) {
  registerActor(globalServiceIdentifier, ALARM_LIGHT_ACTOR_TYPE);
  setupCharacteristicForAlarm(service, ALARM_LIGHT_CHARACTERISTIC_UUID, globalServiceIdentifier);
}

function registerSensor(globalServiceIdentifier, type) {
  console.log('Registering sensor ' + globalServiceIdentifier + ' of type ' + type);
  
  var path = SINGLE_SENSOR_PATH.replace('{id}', globalServiceIdentifier);
  
  var data = {
    type: type
  };
  
  sendRequest(path, data, 'PUT', function (error, response) {
    if (error) {
      console.log(util.format('An error occurred while registering sensor %s of type %s: %s', globalServiceIdentifier, type, error.toString()));
    } else {
      console.log(util.format('Registering sensor %s of type %s: finished with status %s.', globalServiceIdentifier, type, response.statusCode));
    }
  });
}

function setupCharacteristicForSensorReading(service, characteristicUuid, globalServiceIdentifier) {
  service.discoverCharacteristics([characteristicUuid], function characteristicDiscovered(error, characteristics) {
    characteristics.forEach(function(characteristic) {
      characteristic.on('data', function(data) {
        handleSensorUpdate(globalServiceIdentifier, data);
      });

      //characteristic.notify(true);
      sensorCharacteristics[globalServiceIdentifier] = characteristic;
    });
  });
}

function setupCharacteristicForAlarm(service, characteristicUuid, globalServiceIdentifier) {
/*  service.on('characteristicsDiscover', function characteristicDiscovered(characteristics) {
    characteristics.forEach(function (characteristic) {
      console.log("Found characteristic " + characteristic.uuid);
    });
  });
  service.discoverCharacteristics();
*/  
  service.discoverCharacteristics([characteristicUuid], function characteristicDiscovered(error, characteristics) {
    characteristics.forEach(function(characteristic) {
      alarmCharacteristics[globalServiceIdentifier] = characteristic;
    });

    //if (characteristics && characteristics.length > 0) {
    //  var characteristic = characteristics[0];
    //  var toggle = 0;
      
    //  console.log("Setting up characteristic " + characteristic.uuid + ' as alarm...');

    //  setInterval(function() {
    //    toggle = toggle === 1 ? 0 : 1;
    //    console.log("Toggling characteristic " + characteristic.uuid + ' to ' + toggle);
    //    var toggleBuffer = new Buffer(1);
    //    toggleBuffer.writeUInt8(toggle, 0);
    //    characteristic.write(toggleBuffer, false);
    //  }, 2000);
    //}
  });
}

function registerActor(globalServiceIdentifier, type) {
  var path = SINGLE_ACTOR_PATH.replace('{id}', globalServiceIdentifier);
  
  var data = {
    type: type
  };
  
  sendRequest(path, data, 'PUT', function (error, response) {
    if (error) {
      console.log(util.format('An error occurred while registering actor %s of type %s: %s', globalServiceIdentifier, type, error.toString()));
    } else {
      console.log(util.format('Registered actor %s of type %s: response status %s.', globalServiceIdentifier, type, response.statusCode));
    }
  });
}

function sendRequest(path, data, method, callback) {
  var options = {
    host: REMOTE_HOST,
    path: path,
    method: method,
    headers: { 'X-ZUMO-APPLICATION': APPLICATION_KEY }
  };
  
  var req = https.request(options, function responseReceived(response) {
    callback(null, response);
  });
  
  req.on('error', function errorOccurred(error) {
    callback(error);
  });
  
  if (data !== null) {
    var dataAsJson = JSON.stringify(data);
    console.log('Sending ' + dataAsJson + ' to ' + path + ' via ' + method + '.');
    req.write(dataAsJson);
  }
  
  req.end();
}

function handleSensorUpdate(globalServiceIdentifier, sensorData) {
  var path = SENSOR_MEASUREMENTS_PATH.replace('{id}', globalServiceIdentifier);
  
//  console.log('Read ' + JSON.stringify(sensorData));
  var sensorValue = sensorData.readUInt16LE(0);
  
  var data = {
    value: sensorValue
  };
  
  sendRequest(path, data, 'POST', function (error, response) {
    if (error) {
      console.log(util.format('An error occurred while sending value "%s" to sensor %s: %s', sensorValue, globalServiceIdentifier, error.toString()));
    } else {
      console.log(util.format('Sent value "%s" to sensor %s: response status %s.', sensorValue, globalServiceIdentifier, response.statusCode));
    }
  });
}
