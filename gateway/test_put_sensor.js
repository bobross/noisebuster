var REMOTE_HOST = 'zeatmobileservice.azure-mobile.net';
var SENSOR_PATH = '/api/sensor/{id}';
var SENSOR_MEASUREMENTS_PATH = '/api/sensor/{id}/measurements';
var ACTOR_PATH = '/sensor/{id}';

 var APPLICATION_KEY = "lUeMJPkuUdJYCqRrBgTZMIPrbKmhcM86"
 
var https = require('https');
var util = require('util');

var path = SENSOR_PATH.replace('{id}', 'Thomas4');

var data = {
  type: 'Lunch'
};

sendRequest(path, data, 'PUT', function(error, response) {
  if (error) {
    console.log(util.format('Error: %s', error.toString()));
  } else {
    var output = '';
    response.setEncoding('utf8');

    response.on('data', function (chunk) {
        output += chunk;
    });
  
    response.on('end', function() {
      console.log('Response received (' + response.statusCode + '): ' + output);
    });
  }
});

function sendRequest(path, data, method, callback) {
  var options = {
    host: REMOTE_HOST,
    path: path,
    method: method,
    headers: { 'X-ZUMO-APPLICATION': APPLICATION_KEY }
  };
  
  var req = https.request(options, function responseReceived(response) {
    callback(null, response);
  });
  
  req.on('error', function errorOccurred(error) {
    callback(error);
  });
  
  if (data !== null) {
    console.log(JSON.stringify(data));
    req.write(JSON.stringify(data));
  }
  
  req.end();  
}