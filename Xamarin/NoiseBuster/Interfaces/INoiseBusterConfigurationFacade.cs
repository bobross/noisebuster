﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Interfaces
{
    using System.Runtime.InteropServices;

    using NoiseBuster.Core;

    /// <summary>
    /// The complete interface that provides access to sensor data 
    /// </summary>
    public interface INoiseBusterConfigurationFacade
    {
        Task<List<Sensor>> GetSensors();

        Task<Sensor> UpdateSensor(Sensor sensorToUpdate);

        Task DeleteSensor(Sensor sensorToDelete);

        Task<List<Actor>> GetActors();

        Task<Actor> UpdateActor(Actor actorToUpdate);

        Task DeleteActor(Actor actorToDelete);

        Task<List<Alarm>> GetAlarmsOfActor(Actor targetActor);

        Task<Alarm> AddAlarmToActor(Actor targetActor, Alarm alarmToAdd);

        Task RemoveAlarmFromActor(Actor targetActor, Alarm alarmToRemove);

        Task<List<Alarm>> GetAlarms();

        Task<Alarm> CreateAlarm(Alarm alarmToCreate);

        Task<Alarm> UpdateAlarm(Alarm alarmToUpdate);

        Task DeleteAlarm(Alarm alarmToDelete);

        Task<List<Config>> GetConfigsOfAlarm(Alarm targetAlarm);

        Task<Config> CreateConfigInAlarm(Alarm targetAlarm, Config configToCreate);

        Task<Config> UpdateConfigInAlarm(Alarm targetAlarm, Config configToUpdate);

        Task DeleteConfigFromAlarm(Alarm targetAlarm, Config configToDelete);

        /// <summary>
        /// Returns all measurements filtered
        /// </summary>
        /// <param name="sourceSensorId">Filteres to only return measurements that originated from the specified source</param>
        /// <param name="fromTimestamp">Filters only measurements that are younger than the specified timestamp</param>
        /// <param name="toTimestamp">Filters only measurements that are older than the specified timestamp</param>
        /// <returns></returns>
        Task<List<Measurement>> GetMeasurements(string sourceSensorId, [Optional] long fromTimestamp, [Optional] long toTimestamp);
    }
}
