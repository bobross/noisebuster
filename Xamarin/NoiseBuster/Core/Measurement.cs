﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Core
{
    /// <summary>
    /// The Measurement of a Sensor
    /// </summary>
    public class Measurement
    {
        public DateTime Timestamp { get; set; }

        public int Value { get; set; }
    }
}
