﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Core
{
    public class Sensor
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Unit { get; set; }

        public int? Location { get; set; }

        #region auto-generated
        public override string ToString()
        {
            return string.Format("Sensor[Id: {0}, Location: {1}, Name: {2}, Type: {3}, Unit: {4}]", Id, Location, Name, Type, Unit);
        }

        protected bool Equals(Sensor other)
        {
            return string.Equals(Id, other.Id) && string.Equals(Name, other.Name) && string.Equals(Type, other.Type) && string.Equals(Unit, other.Unit) && Location == other.Location;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return Equals((Sensor)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Type != null ? Type.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Unit != null ? Unit.GetHashCode() : 0);
                if(Location.HasValue)
                    hashCode = (hashCode * 397) ^ Location.Value;
                return hashCode;
            }
        }

        public static bool operator ==(Sensor left, Sensor right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Sensor left, Sensor right)
        {
            return !Equals(left, right);
        }
        #endregion
    }
}
