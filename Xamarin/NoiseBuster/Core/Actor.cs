﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Core
{
    public class Actor
    {
        public string Id { get; set; }
        public string Type { get; set; }
        public string Name { get; set; }

        protected bool Equals(Actor other)
        {
            return string.Equals(Id, other.Id) && string.Equals(Type, other.Type) && string.Equals(Name, other.Name);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return Equals((Actor)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Type != null ? Type.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static bool operator ==(Actor left, Actor right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Actor left, Actor right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return string.Format("Actor[Id: {0}, Name: {1}, Type: {2}]", Id, Name, Type);
        }
    }
}
