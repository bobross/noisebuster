﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Core
{
    /// <summary>
    /// The configuration which represents the logical connection between a sensor and an alarm
    /// </summary>
    public class Config
    {
        public int Threshold { get; set; }
        public string SensorId { get; set; }
        public string Id { get; set; }
    }
}
