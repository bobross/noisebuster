﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Core
{
    public class Alarm
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public bool Active { get; set; }

        protected bool Equals(Alarm other)
        {
            return string.Equals(Id, other.Id) && string.Equals(Name, other.Name) && Active.Equals(other.Active);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            if (obj.GetType() != this.GetType())
            {
                return false;
            }
            return Equals((Alarm)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Id != null ? Id.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ Active.GetHashCode();
                return hashCode;
            }
        }

        public static bool operator ==(Alarm left, Alarm right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Alarm left, Alarm right)
        {
            return !Equals(left, right);
        }

        public override string ToString()
        {
            return string.Format("Active: {0}, Id: {1}, Name: {2}", Active, Id, Name);
        }
    }
}
