﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Design
{
    using NoiseBuster.Core;
    using NoiseBuster.Interfaces;

    public class DesignNoiseBusterConfigurationFacade : INoiseBusterConfigurationFacade
    {
        private readonly List<Sensor> Sensors;

        private readonly List<Alarm> Alarms;

        private readonly List<Actor> Actors;

        private readonly Dictionary<string, List<Measurement>> Measurements;

        public static Lazy<DesignNoiseBusterConfigurationFacade> Instance = new Lazy<DesignNoiseBusterConfigurationFacade>(() => new DesignNoiseBusterConfigurationFacade());

        private DesignNoiseBusterConfigurationFacade()
        {
            Sensors = new List<Sensor>
                          {
                              new Sensor { Id = "Sensor1Id", Location = 1, Name = "Sensor 1", Type = "Light-Sensor", Unit = "Lux" },
                              new Sensor { Id = "Sensor2Id", Location = 2, Name = "Sensor 2", Type = "Light-Sensor", Unit = "Lux" },
                              new Sensor { Id = "Sensor3Id", Location = 3, Name = "Sensor 3", Type = "Noise-Sensor", Unit = "dB" },
                              new Sensor { Id = "Sensor4Id", Location = 4, Name = "", Type = "Noise-Sensor", Unit = "dB" },
                          };

            Alarms = new List<Alarm>
                         {
                             new Alarm { Id = "Alarm1Id", Name = "Alarm 1", Active = true },
                             new Alarm { Id = "Alarm2Id", Name = "Alarm 2", Active = false },
                             new Alarm { Id = "Alarm3Id", Name = "Alarm 3", Active = false },
                         };

            Actors = new List<Actor>
                         {
                             new Actor { Id = "Actor1Id", Name = "Light bulp", Type = "Light" },
                             new Actor { Id = "Actor2Id", Name = "Beeper All", Type = "Sound" },
                         };

            Measurements = new Dictionary<string, List<Measurement>>();

            Measurements.Add(Sensors[0].Id, new List<Measurement>
                        {
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 50, 00), Value = 1000 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 50, 10), Value = 1000 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 50, 20), Value = 2000 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 50, 30), Value = 2000 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 50, 40), Value = 7000 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 50, 50), Value = 7000 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 12, 51, 00), Value = 7000 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 13, 50, 00), Value = 7000 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 14, 50, 00), Value = 1000 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 12, 15, 50, 00), Value = 1000 }
                        });

            Measurements.Add(Sensors[1].Id, new List<Measurement>
                        {
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 15, 15, 00), Value = 15 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 15, 25, 00), Value = 16 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 15, 35, 00), Value = 30 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 15, 45, 00), Value = 100 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 15, 55, 00), Value = 80 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 16, 5, 00), Value = 70 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 16, 15, 00), Value = 60 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 16, 25, 00), Value = 80 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 16, 35, 00), Value = 92 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 16, 45, 00), Value = 60 },
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 16, 55, 00), Value = 54 }, 
                            new Measurement { Timestamp = new DateTime(2015, 06, 13, 17, 5, 00), Value = 92 },
                        });

        }

        public Task<List<Sensor>> GetSensors()
        {
            return Task.FromResult(Sensors);
        }

        public Task<Sensor> UpdateSensor(Sensor sensor)
        {
            for (int i = 0; i < Sensors.Count; i++)
            {
                if (Sensors[i].Id == sensor.Id)
                {
                    Sensors[i] = sensor;
                }
            }
            return Task.FromResult(sensor);
        }

        public async Task DeleteSensor(Sensor sensor)
        {
            var sensorsToDelete = Sensors.Where(s => s.Id == sensor.Id).ToList();
            foreach (var sensorToDelete in sensorsToDelete)
            {
                Sensors.Remove(sensorToDelete);
            }            
        }

        public Task<Actor> UpdateActor(Actor actor)
        {
            throw new NotImplementedException();
        }

        public Task DeleteActor(Actor actorToDelete)
        {
            throw new NotImplementedException();
        }

        public Task<List<Alarm>> GetAlarmsOfActor(Actor targetActor)
        {
            throw new NotImplementedException();
        }

        public Task RemoveAlarmFromActor(Actor targetActor, Alarm alarmToRemove)
        {
            throw new NotImplementedException();
        }

        public Task<Alarm> AddAlarmToActor(Actor targetActor, Alarm alarmToAdd)
        {
            throw new NotImplementedException();
        }

        public Task<List<Alarm>> GetAlarms()
        {
            return Task.FromResult(Alarms);
        }

        public Task<Alarm> CreateAlarm(Alarm alarm)
        {
            throw new NotImplementedException();
        }

        public Task<Alarm> UpdateAlarm(Alarm alarm)
        {
            throw new NotImplementedException();
        }

        public Task DeleteAlarm(Alarm alarm)
        {
            throw new NotImplementedException();
        }

        public Task<List<Actor>> GetActors()
        {
            return Task.FromResult(Actors);
        }

        public Task<List<Config>> GetConfigsOfAlarm(Alarm targetAlarm)
        {
            if (targetAlarm.Id == "Alarm1Id")
            {
                return Task.FromResult(new List<Config> { new Config { Threshold = 80, SensorId = Sensors[2].Id } });
            }
            if (targetAlarm.Id == "Alarm2Id")
            {
                return Task.FromResult(new List<Config> { new Config { Threshold = 6000, SensorId = Sensors[1].Id } });
            }
            return Task.FromResult(new List<Config>());
        }

        public Task<Config> CreateConfigInAlarm(Alarm targetAlarm, Config configToCreate)
        {
            throw new NotImplementedException();
        }

        public Task<Config> UpdateConfigInAlarm(Alarm targetAlarm, Config configToUpdate)
        {
            throw new NotImplementedException();
        }

        public Task DeleteConfigFromAlarm(Alarm targetAlarm, Config configToDelete)
        {
            throw new NotImplementedException();
        }

        public Task<List<Measurement>> GetMeasurements(string sourceSensorId, long fromTimestamp = 0, long toTimestamp = long.MaxValue)
        {
            if (Measurements.ContainsKey(sourceSensorId))
            {
                List<Measurement> measurements = Measurements[sourceSensorId];
                List<Measurement> result = new List<Measurement>();
                foreach (var measurement in measurements)
                {
                    if (measurement.Timestamp.Ticks >= fromTimestamp && measurement.Timestamp.Ticks <= toTimestamp)
                        result.Add(measurement);
                }
                return Task.FromResult(result);
            }
            return Task.FromResult(new List<Measurement>());
        }
    }
}
