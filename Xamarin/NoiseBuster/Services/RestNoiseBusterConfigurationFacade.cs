﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoiseBuster.Services
{
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    using NoiseBuster.Core;
    using NoiseBuster.Interfaces;

    public class RestNoiseBusterConfigurationFacade : INoiseBusterConfigurationFacade
    {
        private readonly HttpClient _httpClient;

        private const string BaseUrl = "https://zeatmobileservice.azure-mobile.net/api";

        public RestNoiseBusterConfigurationFacade()
        {
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Add("X-ZUMO-APPLICATION", "lUeMJPkuUdJYCqRrBgTZMIPrbKmhcM86");
        }

        public async Task<List<Sensor>> GetSensors()
        {
            return await GetAllItems<Sensor>("sensor/");
        }

        public async Task<Sensor> UpdateSensor(Sensor sensorToUpdate)
        {
            var httpContent = GetHttpContentFromObject(sensorToUpdate);
            var requestUri = string.Format("{0}/sensor/{1}", BaseUrl, sensorToUpdate.Id);
            return await UpdateItem<Sensor>(requestUri, httpContent);
        }

        public async Task DeleteSensor(Sensor sensorToDelete)
        {
            var requestUri = string.Format("{0}/sensor/{1}", BaseUrl, sensorToDelete.Id);
            await DeleteItem(requestUri);
        }

        public async Task<List<Actor>> GetActors()
        {
            return await GetAllItems<Actor>("actor/");
        }

        public async Task<Actor> UpdateActor(Actor actorToUpdate)
        {
            var httpContent = GetHttpContentFromObject(actorToUpdate);
            var requestUri = string.Format("{0}/actor/{1}", BaseUrl, actorToUpdate.Id);
            return await UpdateItem<Actor>(requestUri, httpContent);
        }

        public async Task DeleteActor(Actor actorToDelete)
        {
            var requestUri = string.Format("{0}/actor/{1}", BaseUrl, actorToDelete.Id);
            await DeleteItem(requestUri);
        }

        public async Task<List<Alarm>> GetAlarmsOfActor(Actor targetActor)
        {
            var requestUri = string.Format("{0}/actor/{1}/alarm", BaseUrl, targetActor.Id);
            return await GetAllItems<Alarm>(requestUri);
        }

        public async Task RemoveAlarmFromActor(Actor targetActor, Alarm alarmToRemove)
        {
            var requestUri = string.Format("{0}/actor/{1}/alarm/{2}", BaseUrl, targetActor.Id, alarmToRemove.Id);
            await DeleteItem(requestUri);
        }

        public async Task<Alarm> AddAlarmToActor(Actor targetActor, Alarm alarmToAdd)
        {
            var httpContent = GetHttpContentFromObject(alarmToAdd);
            var requestUri = string.Format("{0}/actor/{1}/alarm", BaseUrl, targetActor.Id);
            return await CreateItem<Alarm>(requestUri, httpContent);
        }

        public async Task<List<Alarm>> GetAlarms()
        {
            return await GetAllItems<Alarm>("alarm/");
        }

        public async Task<Alarm> CreateAlarm(Alarm alarmToCreate)
        {
            var httpContent = GetHttpContentFromObject(alarmToCreate);
            var requestUri = string.Format("{0}/alarm/", BaseUrl);
            return await CreateItem<Alarm>(requestUri, httpContent);
        }

        public async Task<Alarm> UpdateAlarm(Alarm alarmToUpdate)
        {
            var httpContent = GetHttpContentFromObject(alarmToUpdate);
            var requestUri = string.Format("{0}/alarm/{1}", BaseUrl, alarmToUpdate.Id);
            return await UpdateItem<Alarm>(requestUri, httpContent);
        }

        public async Task DeleteAlarm(Alarm alarmToDelete)
        {
            var requestUri = string.Format("{0}/alarm/{1}", BaseUrl, alarmToDelete.Id);
            await DeleteItem(requestUri);
        }

        public async Task<List<Config>> GetConfigsOfAlarm(Alarm targetAlarm)
        {
            return await GetAllItems<Config>("alarm/" + targetAlarm.Id + "/config/");
        }

        public async Task<Config> CreateConfigInAlarm(Alarm targetAlarm, Config configToCreate)
        {
            var httpContent = GetHttpContentFromObject(configToCreate);
            var requestUri = string.Format("{0}/alarm/{1}/config/", BaseUrl, targetAlarm.Id);
            return await CreateItem<Config>(requestUri, httpContent);
        }

        public async Task<Config> UpdateConfigInAlarm(Alarm targetAlarm, Config configToUpdate)
        {
            var httpContent = GetHttpContentFromObject(configToUpdate);
            var requestUri = string.Format("{0}/alarm/{1}/config/{2}/", BaseUrl, targetAlarm.Id, configToUpdate.Id);
            return await UpdateItem<Config>(requestUri, httpContent);
        }

        public async Task DeleteConfigFromAlarm(Alarm targetAlarm, Config configToDelete)
        {
            var requestUri = string.Format("{0}/alarm/{1}/config/{2}", BaseUrl, targetAlarm.Id, configToDelete.Id);
            await DeleteItem(requestUri);
        }

        public async Task<List<Measurement>> GetMeasurements(string sourceSensorId, long fromTimestamp = 0, long toTimestamp = 0)
        {
            string urlAppendix = string.Format("sensor/{0}/measurement", sourceSensorId);
            return await GetAllItems<Measurement>(urlAppendix);
        }

        #region Helper methods

        private async Task<T> CreateItem<T>(string requestUri, HttpContent httpContent)
        {
            var responseMessage = await _httpClient.PostAsync(requestUri, httpContent);
            var result = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.StatusCode == HttpStatusCode.Created)
            {
                return JsonConvert.DeserializeObject<T>(result);
            }

            throw new Exception("Error - " + responseMessage.RequestMessage);
        }

        private async Task<T> UpdateItem<T>(string requestUri, HttpContent httpContent)
        {
            var responseMessage = await _httpClient.PutAsync(requestUri, httpContent);
            var result = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<T>(result);
            }

            throw new Exception("Error - " + responseMessage.RequestMessage);
        }

        private async Task DeleteItem(string requestUri)
        {
            var responseMessage = await _httpClient.DeleteAsync(requestUri);
            if (responseMessage.StatusCode != HttpStatusCode.NoContent)
            {
                throw new Exception("Error - " + responseMessage.RequestMessage);
            }
        }

        private async Task<List<T>> GetAllItems<T>(string urlAppendix)
        {
            var responseMessage = await _httpClient.GetAsync(new Uri(string.Format("{0}/{1}", BaseUrl, urlAppendix)));
            var stringContent = await responseMessage.Content.ReadAsStringAsync();

            if (responseMessage.StatusCode == HttpStatusCode.OK)
            {
                return JsonConvert.DeserializeObject<List<T>>(stringContent);
            }

            throw new Exception("Error - " + responseMessage.RequestMessage);
        }

        private static HttpContent GetHttpContentFromObject<T>(T value)
        {
            string output = JsonConvert.SerializeObject(
                value,
                new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore, ContractResolver = new CamelCasePropertyNamesContractResolver() });
            HttpContent httpContent = new StringContent(output, Encoding.UTF8, "application/json");
            return httpContent;
        }

        #endregion
    }
}
