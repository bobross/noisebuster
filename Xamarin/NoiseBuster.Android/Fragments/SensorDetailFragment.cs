using System;
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using NoiseBuster.Config.Android.Activities;
using NoiseBuster.Core;
using NoiseBuster.Interfaces;

namespace NoiseBuster.Config.Android.Fragments
{
    public class SensorDetailFragment : Fragment
    {
        private readonly Sensor _selectedSensor;
        private readonly INoiseBusterConfigurationFacade _noiseBusterConfigurationFacade;
        private EditText _editTextSensorLocation;
        private EditText _editTextSensorName;
        private EditText _editTextSensorType;
        private EditText _editTextSensorUnit;
        private bool _sensorWasDeleted = false;

        public SensorDetailFragment(Sensor sensor, INoiseBusterConfigurationFacade noiseBusterConfigurationFacade)
        {
            _selectedSensor = sensor;
            _noiseBusterConfigurationFacade = noiseBusterConfigurationFacade;
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            if (container == null)
            {
                // Currently in a layout without a container, so no reason to create our view.
                return null;
            }

            var view = inflater.Inflate(Resource.Layout.SensorDetailView, container, false);                        
            var editTextSensorId = view.FindViewById<EditText>(Resource.Id.editTextSensorId);
            editTextSensorId.Text = _selectedSensor.Id;
            _editTextSensorLocation = view.FindViewById<EditText>(Resource.Id.editTextSensorLocation);
            _editTextSensorLocation.Text = _selectedSensor.Location.HasValue ? _selectedSensor.Location.Value.ToString() : "Unknown";
            _editTextSensorName = view.FindViewById<EditText>(Resource.Id.editTextSensorName);
            _editTextSensorName.Text = _selectedSensor.Name;
            _editTextSensorType = view.FindViewById<EditText>(Resource.Id.editTextSensorType);
            _editTextSensorType.Text = _selectedSensor.Type;
            _editTextSensorUnit = view.FindViewById<EditText>(Resource.Id.editTextSensorUnit);
            _editTextSensorUnit.Text = _selectedSensor.Unit;

            var buttonDeleteSensor = view.FindViewById<Button>(Resource.Id.buttonDeleteSensor);
            buttonDeleteSensor.Click += ButtonDeleteSensorOnClick;

            return view;
        }

        public override void OnPause()
        {
            _selectedSensor.Name = _editTextSensorName.Text;
            int newLocation;
            if(int.TryParse(_editTextSensorLocation.Text, out newLocation))
                _selectedSensor.Location = newLocation;
            _selectedSensor.Type = _editTextSensorType.Text;
            _selectedSensor.Unit = _editTextSensorUnit.Text;

            if (!_sensorWasDeleted)
            {
                _noiseBusterConfigurationFacade.UpdateSensor(_selectedSensor);
            }

            base.OnPause();
        }

        private void ButtonDeleteSensorOnClick(object sender, EventArgs eventArgs)
        {
            _sensorWasDeleted = true;
            _noiseBusterConfigurationFacade.DeleteSensor(_selectedSensor);
            if (Activity is DetailFragmentActivity)
            {
                Activity.Finish();
            }
        }
    }
}