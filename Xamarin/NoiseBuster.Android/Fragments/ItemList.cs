using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;

namespace NoiseBuster.Config.Android.Fragments
{
    using System.Threading.Tasks;

    using NoiseBuster.Core;
    using NoiseBuster.Design;
    using NoiseBuster.Interfaces;

    public class ItemList<TItemType> : ListFragment, IRefreshable
    {
        /// <summary>
        /// The list that will hold the items (e.g. Sensor, Alarm, ...)
        /// </summary>
        private List<TItemType> _items;

        /// <summary>
        /// The factory-method that converts a list of items (and a provided activity) into an IListAdapter that can be used to fill the List-View Widget.
        /// </summary>
        private readonly Func<Activity, List<TItemType>, IListAdapter> _listAdapterFactory;

        /// <summary>
        /// The specific retrieve function that can be used to get the list of all items of the type TItemType
        /// </summary>
        private readonly Func<Task<List<TItemType>>> _retrieveFunction;

        /// <summary>
        /// The callback that will be called when an element is clicked in the list-view.
        /// </summary>
        private readonly Action<TItemType> _onListItemSelected;

        /// <summary>
        /// Creates and initializes a new ItemList which is the abstraction for all list fragments that simply hold a list of items of type TItemType.
        /// </summary>
        /// <param name="listAdapterFactory">The factory-method that converts a list of items (and a provided activity) into an IListAdapter that can be used to fill the List-View Widget.</param>
        /// <param name="retrieveFunction">The specific retrieve function that can be used to get the list of all items of the type TItemType</param>
        /// <param name="onListItemSelected">The callback that will be called when an element is clicked in the list-view.</param>
        public ItemList(Func<Activity, List<TItemType>, IListAdapter> listAdapterFactory, Func<Task<List<TItemType>>> retrieveFunction, Action<TItemType> onListItemSelected)
        {
            _listAdapterFactory = listAdapterFactory;
            _retrieveFunction = retrieveFunction;
            _onListItemSelected = onListItemSelected;
        }

        public override void OnResume()
        {
            base.OnResume();
            if (_items != null) 
                ListAdapter = _listAdapterFactory(Activity, _items);
        }

        public override void OnListItemClick(ListView l, View v, int position, long id)
        {
            if(_onListItemSelected != null)
                _onListItemSelected(_items[position]);

            //string t = _items[position].ToString();
            //Toast.MakeText(Activity, t, ToastLength.Short).Show();
        }

        public async void Refresh()
        {
            try
            {
                _items = await _retrieveFunction();
            }
            catch (Exception e)
            {
                Toast.MakeText(Activity, "Error when loading data. " + e.Message, ToastLength.Long);
            }

            if (Activity != null)
                ListAdapter = _listAdapterFactory(Activity, _items);
        }
    }
}