using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using NoiseBuster.Core;

namespace NoiseBuster.Config.Android.Adapters
{
    public class AlarmAdapter : BaseAdapter<Alarm>
    {
        readonly List<Alarm> _items;
        readonly Activity _context;

        public AlarmAdapter(Activity context, List<Alarm> items)
        {
            _context = context;
            _items = items;
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Alarm this[int position]
        {
            get { return _items[position]; }
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
                view = _context.LayoutInflater.Inflate(Resource.Layout.AlarmListItem, null);
            view.FindViewById<TextView>(Resource.Id.textViewAlarmName).Text = _items[position].Name + " (" + _items[position] + ")";
            return view;
        }
    }
}