using System.Collections.Generic;
using Android.App;
using Android.Views;
using Android.Widget;
using NoiseBuster.Core;

namespace NoiseBuster.Config.Android.Adapters
{
    public class ActorAdapter : BaseAdapter<Actor>
    {
        readonly List<Actor> _items;
        readonly Activity _context;

        public ActorAdapter(Activity context, List<Actor> items)
        {
            _context = context;
            _items = items;

        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Actor this[int position]
        {
            get { return _items[position]; }
        }

        public override int Count
        {
            get { return _items.Count; }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View view = convertView; // re-use an existing view, if one is available
            if (view == null) // otherwise create a new one
                view = _context.LayoutInflater.Inflate(Resource.Layout.ActorListItem, null);
            view.FindViewById<TextView>(Resource.Id.textViewActorName).Text = _items[position].Name + " (" + _items[position] + ")";
            return view;
        }
    }
}