using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Newtonsoft.Json;
using NoiseBuster.Config.Android.Fragments;
using NoiseBuster.Core;
using NoiseBuster.Design;
using NoiseBuster.Interfaces;

namespace NoiseBuster.Config.Android.Activities
{
    [Activity(Label = "Edit sensor details")]
    public class DetailFragmentActivity : Activity
    {
        private INoiseBusterConfigurationFacade _service;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            //_service = new RestNoiseBusterConfigurationFacade();
            _service = DesignNoiseBusterConfigurationFacade.Instance.Value;
            SetContentView(Resource.Layout.DetailFragmentActivity);

            ActionBar.SetHomeButtonEnabled(true);
            ActionBar.SetDisplayHomeAsUpEnabled(true);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                case global::Android.Resource.Id.Home:
                    Finish();
                    return true;
            }
            return false;
        }

        protected override void OnResume()
        {
            base.OnResume();

            if (Intent.Extras != null && Intent.Extras.ContainsKey("sensor"))
            {
                string serializedSensor = Intent.Extras.GetString("sensor");
                Sensor selectedSensor = JsonConvert.DeserializeObject<Sensor>(serializedSensor);
                SensorDetailFragment fragment = new SensorDetailFragment(selectedSensor, _service);
                var transaction = FragmentManager.BeginTransaction();
                transaction.Add(Resource.Id.detailFrameContainer, fragment);
                transaction.Commit();
            }
        }
    }
}