﻿using System;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Util;
using Android.Views;
using Newtonsoft.Json;
using NoiseBuster.Config.Android.Adapters;
using NoiseBuster.Config.Android.Fragments;
using NoiseBuster.Core;
using NoiseBuster.Design;
using NoiseBuster.Interfaces;

namespace NoiseBuster.Config.Android.Activities
{
    [Activity(Label = "NoiseBuster", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        Fragment[] _fragments;
        private INoiseBusterConfigurationFacade _service;
        private bool _isDualPane;
        private Fragment _currentDetailFragment;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            //_service = new RestNoiseBusterConfigurationFacade();
            _service = DesignNoiseBusterConfigurationFacade.Instance.Value;
             
            CreateTabs();
        }

        private void CreateTabs()
        {
            SetContentView(Resource.Layout.Main);
            var detailsFrame = FindViewById<View>(Resource.Id.frameLayoutDetails);
            _isDualPane = detailsFrame != null && detailsFrame.Visibility == ViewStates.Visible;

            ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            _fragments = new Fragment[]
                         {
                             new ItemList<Sensor>(
                                 (activity, list) => new SensorAdapter(activity, list),
                                 () => _service.GetSensors(), 
                                 OnSensorItemSelected), 
                             new ItemList<Actor>(
                                 (activity, list) => new ActorAdapter(activity, list),
                                 () => _service.GetActors(),
                                 OnActorItemSelected), 
                             new ItemList<Alarm>(
                                 (activity, list) => new AlarmAdapter(activity, list),
                                 () => _service.GetAlarms(),
                                 OnAlarmItemSelected),     
                         };

            AddTabToActionBar(Resource.String.TabSensorsTitle, Resource.Drawable.ic_action_speakers);
            AddTabToActionBar(Resource.String.TabActorsTitle, Resource.Drawable.ic_action_whats_on);
            AddTabToActionBar(Resource.String.TabAlarmsTitle, Resource.Drawable.ic_action_sessions); 
        }

        private void OnSensorItemSelected(Sensor sensor)
        {
            if (_isDualPane)
            {
                _currentDetailFragment = new SensorDetailFragment(sensor, _service);
                var transaction = FragmentManager.BeginTransaction();
                transaction.Replace(Resource.Id.frameLayoutDetails, _currentDetailFragment);
                transaction.Commit();
            }
            else
            {
                Intent detailActivityIntent = new Intent(this, typeof(DetailFragmentActivity));
                Bundle options = new Bundle();
                string serializedSensor = JsonConvert.SerializeObject(sensor, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                options.PutString("sensor", serializedSensor);               
                detailActivityIntent.PutExtras(options);
                StartActivity(detailActivityIntent, options);                
            }            
        }

        private void OnActorItemSelected(Actor actor)
        {
            
        }

        private void OnAlarmItemSelected(Alarm alarm)
        {
            
        }



        void AddTabToActionBar(int labelResourceId, int iconResourceId)
        {
            ActionBar.Tab tab = ActionBar.NewTab()
                                         .SetText(labelResourceId)
                                         .SetIcon(iconResourceId);
            tab.TabSelected += OnTabSelected;
            tab.TabReselected += OnTabReselected;
            ActionBar.AddTab(tab); 
        }

        private void OnTabReselected(object sender, ActionBar.TabEventArgs e)
        {
            ActionBar.Tab tab = (ActionBar.Tab)sender;
            Log.Debug("Tabs", "The tab {0} has been selected.", tab.Text);
            ((IRefreshable)_fragments[tab.Position]).Refresh();

            if (_isDualPane && _currentDetailFragment != null)
            {
                var transaction = FragmentManager.BeginTransaction();
                transaction.Remove(_currentDetailFragment);
                transaction.Commit();
            }
        }

        void OnTabSelected(object sender, ActionBar.TabEventArgs tabEventArgs)
        {
            ActionBar.Tab tab = (ActionBar.Tab)sender;
            Log.Debug("Tabs", "The tab {0} has been selected.", tab.Text);
            Fragment frag = _fragments[tab.Position];
            tabEventArgs.FragmentTransaction.Replace(Resource.Id.frameLayoutList, frag);
            ((IRefreshable)frag).Refresh();

            if (_isDualPane && _currentDetailFragment != null)
            {
                var transaction = FragmentManager.BeginTransaction();
                transaction.Remove(_currentDetailFragment);                
                transaction.Commit();
            }
        }
    }
}

