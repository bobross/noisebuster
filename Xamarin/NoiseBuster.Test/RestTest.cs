﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Test
{
    using NoiseBuster.Core;
    using NoiseBuster.Design;
    using NoiseBuster.Interfaces;
    using NoiseBuster.Services;

    using NUnit.Framework;

    [TestFixture]
    public class RestTest
    {

        [Test]
        public void TestNoiseBusterFacade_CallAllGetMethods_ExpectNonEmptyLists()
        {
            // Arrange
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();

            // Act
            var sensors = service.GetSensors().Result;
            var actors = service.GetActors().Result;
            var alarms = service.GetAlarms().Result;
            var configs = service.GetConfigsOfAlarm(alarms[0]).Result;
            var alarmOfActor0 = service.GetAlarmsOfActor(actors[0]).Result;
            var measurements = service.GetMeasurements(sensors[0].Id);

            // Assert
            Assert.That(sensors, Is.Not.Empty);
            Assert.That(actors, Is.Not.Empty);
            Assert.That(alarms, Is.Not.Empty);
            Assert.That(configs, Is.Not.Empty);
            Assert.That(alarmOfActor0, Is.Not.Empty);
            Assert.That(measurements, Is.Not.Empty);
        }

        [Test]
        public void TestUpdateSensor()
        {
            // Arrange
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();
            const string OriginalName = "TestSensor1";
            const string ChangedName = "TestSensor1_changed";
            var existingSensor = new Sensor { Id = "EC8C8496-6204-4A1F-A6ED-E119E4AA32C8", Name = OriginalName, Type = "type1", Unit = "unit1" };
            
            // Act
            existingSensor.Name = ChangedName;
            var sensors = service.UpdateSensor(existingSensor).Result;

            // Assert
            Assert.That(sensors.Name, Is.EqualTo(ChangedName));

            // Cleanup
            existingSensor.Name = OriginalName;
            service.UpdateSensor(existingSensor);
        }


        [Test]
        public void TestUpdateActor()
        {
            // Arrange
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();
            const string OriginalName = "TestActor1";
            const string ChangedName = "TestActor1_changed";
            var existingActor = new Actor { Id = "??TODO??", Name = OriginalName, Type = "type1" };
            
            // Act
            existingActor.Name = ChangedName;
            var sensors = service.UpdateActor(existingActor).Result;

            // Assert
            Assert.That(sensors.Name, Is.EqualTo(ChangedName));

            // Cleanup
            existingActor.Name = OriginalName;
            service.UpdateActor(existingActor);
        }

        [Test]
        public void AddRemoveAlarmFromActor()
        {
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();
            var alarm = new Alarm { Id = "??TODO??", Name = "UnitTestAlarm2", Active = false };
            Actor existingActor = new Actor { Id = "??TODO??", Name = "", Type = "" };

            var createdAlarm = service.AddAlarmToActor(existingActor, alarm).Result;
            var newListOfAlarms = service.GetAlarmsOfActor(existingActor).Result;
            Assert.That(createdAlarm.Id, Is.Not.Null);
            Assert.That(newListOfAlarms, Is.Not.Empty);
            Assert.That(newListOfAlarms, Contains.Item(createdAlarm));

            service.RemoveAlarmFromActor(existingActor, createdAlarm).Wait();
            var finalListOfAlarms = service.GetAlarmsOfActor(existingActor).Result;
            Assert.That(newListOfAlarms, Is.Not.Null);
            Assert.That(createdAlarm, Is.Not.SubsetOf(finalListOfAlarms));
        }

        [Test]
        public void Cud_Alarm()
        {
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();
            var alarm = new Alarm { Name = "UnitTestAlarm1", Active = false };

            var createdAlarm = service.CreateAlarm(alarm).Result;
            Assert.That(createdAlarm.Id, Is.Not.Null);
            Assert.That(createdAlarm.Active, Is.False);

            createdAlarm.Active = true;
            var updatedAlarm = service.UpdateAlarm(createdAlarm).Result;
            Assert.That(createdAlarm.Id, Is.EqualTo(updatedAlarm.Id));
            Assert.That(createdAlarm.Active, Is.True);

            service.DeleteAlarm(updatedAlarm).Wait();
        }

        [Test]
        public void Cud_Config()
        {
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();
            var existingAlarm = new Alarm() { Id = "DA32548B-6F70-4D0E-A97D-34CFE69B9ABC" };
            var config = new Config { SensorId = "EC8C8496-6204-4A1F-A6ED-E119E4AA32C8", Threshold = 80 };

            var createdConfig = service.CreateConfigInAlarm(existingAlarm, config).Result;
            Assert.That(createdConfig.SensorId, Is.Not.Null);
            Assert.That(createdConfig.Threshold, Is.EqualTo(80));
            Assert.That(createdConfig.Id, Is.Not.Null);

            createdConfig.Threshold = 90;
            var updatedConfig = service.UpdateConfigInAlarm(existingAlarm, createdConfig).Result;
            Assert.That(createdConfig.SensorId, Is.EqualTo(updatedConfig.SensorId));
            Assert.That(createdConfig.Threshold, Is.EqualTo(90));

            service.DeleteConfigFromAlarm(existingAlarm, updatedConfig).Wait();
        }

        [Test]
        public void TestMeasurementCreation_Loop()
        {
            // Arrange
            INoiseBusterConfigurationFacade service = new RestNoiseBusterConfigurationFacade();

            // Act
            Random r = new Random();
            for (int i = 0; i < 20; i++)
            {
                var val = r.Next(10000, 100000);
                //var measurement = service.CreateMeasurement("6950D278-5489-4849-84A3-B0754C22B9CD", val, DateTime.UtcNow);    
            }
            
            // Assert
            //Assert.That(measurement, Is.Not.Null);
        }
    }
}
