﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoiseBuster.Test
{
    using NoiseBuster.Design;
    using NoiseBuster.Interfaces;

    using NUnit.Framework;

    [TestFixture]
    public class DesignTest
    {

        [Test]
        public void TestDesignService_NonEmptyLists()
        {
            // Arrange
            INoiseBusterConfigurationFacade service = DesignNoiseBusterConfigurationFacade.Instance.Value;

            // Act
            var sensors = service.GetSensors().Result;
            var actors = service.GetActors().Result;
            var alarms = service.GetAlarms().Result;

            // Assert
            Assert.That(sensors, Is.Not.Empty);
            Assert.That(actors, Is.Not.Empty);
            Assert.That(alarms, Is.Not.Empty);
        } 
        
        [Test]
        public void TestDesignService_Measurements()
        {
            // Arrange
            INoiseBusterConfigurationFacade service = DesignNoiseBusterConfigurationFacade.Instance.Value;

            // Act
            var sensors = service.GetSensors();
            var measurements = service.GetMeasurements("Sensor1Id", new DateTime(2015, 06, 12, 12, 50, 1).Ticks, long.MaxValue).Result;
            
            // Assert
            Assert.That(measurements, Is.Not.Empty);
            Assert.That(measurements.Count, Is.EqualTo(9));
        }
    }
}
