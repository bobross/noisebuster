/**************************************************************************************************
  Filename:       simpleGATTprofile.c
  Revised:        $Date: 2014-12-23 09:30:19 -0800 (Tue, 23 Dec 2014) $
  Revision:       $Revision: 41572 $

  Description:    This file contains the Simple GATT profile sample GATT service 
                  profile for use with the BLE sample application.

  Copyright 2010 - 2014 Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, 
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE, 
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com. 
**************************************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include <string.h>

#include "bcomdef.h"
#include "OSAL.h"
#include "linkdb.h"
#include "att.h"
#include "gatt.h"
#include "gatt_uuid.h"
#include "gattservapp.h"
#include "gapbondmgr.h"

#include "alarm_service.h"

/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

#define SERVAPP_NUM_ATTR_SUPPORTED        17

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */
// Alarm Service UUID
CONST uint8 alarmServiceServUUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(ALARM_SERVICE_SERV_UUID), HI_UINT16(ALARM_SERVICE_SERV_UUID)
};

// Characteristic Led 1 UUID
CONST uint8 alarmServiceLed1UUID[ATT_BT_UUID_SIZE] =
{ 
  LO_UINT16(ALARM_SERVICE_LED1_UUID), HI_UINT16(ALARM_SERVICE_LED1_UUID)
};

// Characteristic Led 2 UUID
CONST uint8 alarmServiceLed2UUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(ALARM_SERVICE_LED2_UUID), HI_UINT16(ALARM_SERVICE_LED2_UUID)
};

// Characteristic Led 3 UUID
CONST uint8 alarmServiceLed3UUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(ALARM_SERVICE_LED3_UUID), HI_UINT16(ALARM_SERVICE_LED3_UUID)
};

// Characteristic Led 4 UUID
CONST uint8 alarmServiceLed4UUID[ATT_BT_UUID_SIZE] =
{
  LO_UINT16(ALARM_SERVICE_LED4_UUID), HI_UINT16(ALARM_SERVICE_LED4_UUID)
};
/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */

static alarmServiceCBs_t *alarmService_AppCBs = NULL;

/*********************************************************************
 * Profile Attributes - variables
 */

// Simple Profile Service attribute
static CONST gattAttrType_t alarmService = { ATT_BT_UUID_SIZE, alarmServiceServUUID };


// Characteristic LED 1 Properties
static uint8 alarmServiceLed1Props = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic LED 1 Value
static uint8 alarmServiceLed1 = 0;

// Characteristic LED 1 User Description
static uint8 alarmServiceLed1UserDesp[17] = "LED 1";

// Characteristic LED 2 Properties
static uint8 alarmServiceLed2Props = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic LED 2 Value
static uint8 alarmServiceLed2 = 0;

// Characteristic LED 2 User Description
static uint8 alarmServiceLed2UserDesp[17] = "LED 2";

// Characteristic LED 3 Properties
static uint8 alarmServiceLed3Props = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic LED 3 Value
static uint8 alarmServiceLed3 = 0;

// Characteristic LED 3 User Description
static uint8 alarmServiceLed3UserDesp[17] = "LED 3";

// Characteristic LED 4 Properties
static uint8 alarmServiceLed4Props = GATT_PROP_READ | GATT_PROP_WRITE;

// Characteristic LED 4 Value
static uint8 alarmServiceLed4 = 0;

// Characteristic LED 4 User Description
static uint8 alarmServiceLed4UserDesp[17] = "LED 4";

/*********************************************************************
 * Profile Attributes - Table
 */

static gattAttribute_t alarmServiceAttrTbl[SERVAPP_NUM_ATTR_SUPPORTED] =
{
  // Simple Profile Service
  { 
    { ATT_BT_UUID_SIZE, primaryServiceUUID }, /* type */
    GATT_PERMIT_READ,                         /* permissions */
    0,                                        /* handle */
    (uint8 *)&alarmService                    /* pValue */
  },

    // Characteristic 1 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &alarmServiceLed1Props
    },

      // Characteristic Value 1
      { 
        { ATT_BT_UUID_SIZE, alarmServiceLed1UUID },
        GATT_PERMIT_READ | GATT_PERMIT_WRITE, 
        0, 
        &alarmServiceLed1
      },

      // Characteristic 1 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        alarmServiceLed1UserDesp
      },      

    // Characteristic 2 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &alarmServiceLed2Props
    },

      // Characteristic Value 2
      { 
        { ATT_BT_UUID_SIZE, alarmServiceLed2UUID },
		GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0, 
        &alarmServiceLed2
      },

      // Characteristic 2 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        alarmServiceLed2UserDesp
      },           
      
    // Characteristic 3 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
      GATT_PERMIT_READ, 
      0,
      &alarmServiceLed3Props
    },

      // Characteristic Value 3
      { 
        { ATT_BT_UUID_SIZE, alarmServiceLed3UUID },
		GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0, 
        &alarmServiceLed3
      },

      // Characteristic 3 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ, 
        0, 
        alarmServiceLed3UserDesp
      },

    // Characteristic 4 Declaration
    { 
      { ATT_BT_UUID_SIZE, characterUUID },
	  GATT_PERMIT_READ | GATT_PERMIT_WRITE,
      0,
      &alarmServiceLed4Props
    },

      // Characteristic Value 4
      { 
        { ATT_BT_UUID_SIZE, alarmServiceLed4UUID },
		GATT_PERMIT_READ | GATT_PERMIT_WRITE,
        0, 
        &alarmServiceLed4
      },

      // Characteristic 4 User Description
      { 
        { ATT_BT_UUID_SIZE, charUserDescUUID },
        GATT_PERMIT_READ,
        0, 
        alarmServiceLed4UserDesp
      },
};

/*********************************************************************
 * LOCAL FUNCTIONS
 */

static bStatus_t alarmService_VerifyWrite( gattAttribute_t *pAttr, uint8 *pValue, uint8 len,
											uint8_t offset );
static bStatus_t alarmService_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                           uint8 *pValue, uint8 *pLen, uint16 offset,
                                           uint8 maxLen, uint8 method );
static bStatus_t alarmService_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                            uint8 *pValue, uint8 len, uint16 offset,
                                            uint8 method );

/*********************************************************************
 * PROFILE CALLBACKS
 */
// Simple Profile Service Callbacks
CONST gattServiceCBs_t alarmServiceCBs =
{
  alarmService_ReadAttrCB,  // Read callback function pointer
  alarmService_WriteAttrCB, // Write callback function pointer
  NULL                       // Authorization callback function pointer
};

/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      AlarmService_AddService
 *
 * @brief   Initializes the Simple Profile service by registering
 *          GATT attributes with the GATT server.
 *
 * @param   services - services to add. This is a bit map and can
 *                     contain more than one service.
 *
 * @return  Success or Failure
 */
bStatus_t AlarmService_AddService( uint32 services )
{
  uint8 status;

  if ( services & ALARM_SERVICE )
  {
    // Register GATT attribute list and CBs with GATT Server App
    status = GATTServApp_RegisterService( alarmServiceAttrTbl,
                                          GATT_NUM_ATTRS( alarmServiceAttrTbl ),
                                          &alarmServiceCBs );
  }
  else
  {
    status = SUCCESS;
  }

  return ( status );
}

/*********************************************************************
 * @fn      AlarmService_RegisterAppCBs
 *
 * @brief   Registers the application callback function. Only call 
 *          this function once.
 *
 * @param   callbacks - pointer to application callbacks.
 *
 * @return  SUCCESS or bleAlreadyInRequestedMode
 */
bStatus_t AlarmService_RegisterAppCBs( alarmServiceCBs_t *appCallbacks )
{
  if ( appCallbacks )
  {
    alarmService_AppCBs = appCallbacks;
    
    return ( SUCCESS );
  }
  else
  {
    return ( bleAlreadyInRequestedMode );
  }
}

/*********************************************************************
 * @fn      AlarmService_SetParameter
 *
 * @brief   Set a Simple Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   len - length of data to write
 * @param   value - pointer to data to write.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t AlarmService_SetParameter( uint8 param, uint8 len, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case ALARM_SERVICE_LED1:
      if ( len == sizeof ( uint8 ) ) 
      {
        alarmServiceLed1 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case ALARM_SERVICE_LED2:
      if ( len == sizeof ( uint8 ) ) 
      {
        alarmServiceLed2 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case ALARM_SERVICE_LED3:
      if ( len == sizeof ( uint8 ) ) 
      {
        alarmServiceLed3 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    case ALARM_SERVICE_LED4:
      if ( len == sizeof ( uint8 ) ) 
      {
        alarmServiceLed4 = *((uint8*)value);
      }
      else
      {
        ret = bleInvalidRange;
      }
      break;

    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn      AlarmService_GetParameter
 *
 * @brief   Get a Simple Profile parameter.
 *
 * @param   param - Profile parameter ID
 * @param   value - pointer to data to put.  This is dependent on
 *          the parameter ID and WILL be cast to the appropriate 
 *          data type (example: data type of uint16 will be cast to 
 *          uint16 pointer).
 *
 * @return  bStatus_t
 */
bStatus_t AlarmService_GetParameter( uint8 param, void *value )
{
  bStatus_t ret = SUCCESS;
  switch ( param )
  {
    case ALARM_SERVICE_LED1:
      *((uint8*)value) = alarmServiceLed1;
      break;

    case ALARM_SERVICE_LED2:
      *((uint8*)value) = alarmServiceLed2;
      break;      

    case ALARM_SERVICE_LED3:
      *((uint8*)value) = alarmServiceLed3;
      break;  

    case ALARM_SERVICE_LED4:
      *((uint8*)value) = alarmServiceLed4;
      break;
      
    default:
      ret = INVALIDPARAMETER;
      break;
  }
  
  return ( ret );
}

/*********************************************************************
 * @fn          alarmService_ReadAttrCB
 *
 * @brief       Read an attribute.
 *
 * @param       connHandle - connection message was received on
 * @param       pAttr - pointer to attribute
 * @param       pValue - pointer to data to be read
 * @param       pLen - length of data to be read
 * @param       offset - offset of the first octet to be read
 * @param       maxLen - maximum length of data to be read
 * @param       method - type of read message
 *
 * @return      SUCCESS, blePending or Failure
 */
static bStatus_t alarmService_ReadAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                           uint8 *pValue, uint8 *pLen, uint16 offset,
                                           uint8 maxLen, uint8 method )
{
  bStatus_t status = SUCCESS;

  // If attribute permissions require authorization to read, return error
  if ( gattPermitAuthorRead( pAttr->permissions ) )
  {
    // Insufficient authorization
    return ( ATT_ERR_INSUFFICIENT_AUTHOR );
  }
  
  // Make sure it's not a blob operation (no attributes in the profile are long)
  if ( offset > 0 )
  {
    return ( ATT_ERR_ATTR_NOT_LONG );
  }
 
  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16( pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch ( uuid )
    {
      // No need for "GATT_SERVICE_UUID" or "GATT_CLIENT_CHAR_CFG_UUID" cases;
      // gattserverapp handles those reads

      // characteristics 1 and 2 have read permissions
      // characteritisc 3 does not have read permissions; therefore it is not
      //   included here
      // characteristic 4 does not have read permissions, but because it
      //   can be sent as a notification, it is included here
      case ALARM_SERVICE_LED1_UUID:
      case ALARM_SERVICE_LED2_UUID:
      case ALARM_SERVICE_LED3_UUID:
      case ALARM_SERVICE_LED4_UUID:
        *pLen = 1;
        pValue[0] = *pAttr->pValue;
        break;

      default:
        // Should never get here! (characteristics 3 and 4 do not have read permissions)
        *pLen = 0;
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    *pLen = 0;
    status = ATT_ERR_INVALID_HANDLE;
  }

  return ( status );
}

static bStatus_t alarmService_VerifyWrite( gattAttribute_t *pAttr, uint8 *pValue, uint8 len,
										   uint8_t offset )
{
	bStatus_t status = SUCCESS;
    //Validate the value
    // Make sure it's not a blob oper
    if ( offset == 0 )
    {
      if ( len != 1 )
      {
        status = ATT_ERR_INVALID_VALUE_SIZE;
      }
    }
    else
    {
      status = ATT_ERR_ATTR_NOT_LONG;
    }

    //Write the value
    if ( status == SUCCESS )
    {
      uint8 *pCurValue = (uint8 *)pAttr->pValue;
      *pCurValue = pValue[0];
    }
    return status;
}

/*********************************************************************
 * @fn      alarmService_WriteAttrCB
 *
 * @brief   Validate attribute data prior to a write operation
 *
 * @param   connHandle - connection message was received on
 * @param   pAttr - pointer to attribute
 * @param   pValue - pointer to data to be written
 * @param   len - length of data
 * @param   offset - offset of the first octet to be written
 * @param   method - type of write message
 *
 * @return  SUCCESS, blePending or Failure
 */
static bStatus_t alarmService_WriteAttrCB( uint16 connHandle, gattAttribute_t *pAttr,
                                            uint8 *pValue, uint8 len, uint16 offset,
                                            uint8 method )
{
  bStatus_t status = SUCCESS;
  uint8 notifyApp = 0xFF;
  
  // If attribute permissions require authorization to write, return error
  if ( gattPermitAuthorWrite( pAttr->permissions ) )
  {
    // Insufficient authorization
    return ( ATT_ERR_INSUFFICIENT_AUTHOR );
  }
  
  if ( pAttr->type.len == ATT_BT_UUID_SIZE )
  {
    // 16-bit UUID
    uint16 uuid = BUILD_UINT16(pAttr->type.uuid[0], pAttr->type.uuid[1]);
    switch (uuid)
    {
      case ALARM_SERVICE_LED1_UUID:
        status = alarmService_VerifyWrite(pAttr, pValue, len, offset);
        if (status == SUCCESS)
        {
          if (pAttr->pValue == &alarmServiceLed1)
          {
            notifyApp = ALARM_SERVICE_LED1;
          }
        }
        break;

      case ALARM_SERVICE_LED2_UUID:
        status = alarmService_VerifyWrite(pAttr, pValue, len, offset);
        if (status == SUCCESS)
        {
          if (pAttr->pValue == &alarmServiceLed2)
          {
            notifyApp = ALARM_SERVICE_LED2;
          }
        }
        break;

      case ALARM_SERVICE_LED3_UUID:
        status = alarmService_VerifyWrite(pAttr, pValue, len, offset);
        if (status == SUCCESS)
        {
          if (pAttr->pValue == &alarmServiceLed3)
          {
            notifyApp = ALARM_SERVICE_LED3;
          }
        }
        break;

      case ALARM_SERVICE_LED4_UUID:
        status = alarmService_VerifyWrite(pAttr, pValue, len, offset);
        if (status == SUCCESS)
        {
          if (pAttr->pValue == &alarmServiceLed4)
          {
            notifyApp = ALARM_SERVICE_LED4;
          }
        }
        break;

      case GATT_CLIENT_CHAR_CFG_UUID:
        status = GATTServApp_ProcessCCCWriteReq( connHandle, pAttr, pValue, len,
                                                 offset, GATT_CLIENT_CFG_NOTIFY );
        break;
        
      default:
        // Should never get here! (characteristics 2 and 4 do not have write permissions)
        status = ATT_ERR_ATTR_NOT_FOUND;
        break;
    }
  }
  else
  {
    // 128-bit UUID
    status = ATT_ERR_INVALID_HANDLE;
  }

  // If a charactersitic value changed then callback function to notify application of change
  if ( (notifyApp != 0xFF ) && alarmService_AppCBs && alarmService_AppCBs->pfnAlarmServiceChange )
  {
    alarmService_AppCBs->pfnAlarmServiceChange( notifyApp );
  }
  
  return ( status );
}

/*********************************************************************
*********************************************************************/
