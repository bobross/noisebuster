/*******************************************************************************
  Filename:       ambient_light.c
  Revised:        $Date: 2013-11-06 17:27:44 +0100 (on, 06 nov 2013) $
  Revision:       $Revision: 35922 $

  Description:    This file contains the SmartRf06 ambient light sensor part,
                  for use with the TI Bluetooth Low Energy Protocol Stack.

  Copyright 2015  Texas Instruments Incorporated. All rights reserved.

  IMPORTANT: Your use of this Software is limited to those specific rights
  granted under the terms of a software license agreement between the user
  who downloaded the software, his/her employer (which must be your employer)
  and Texas Instruments Incorporated (the "License").  You may not use this
  Software unless you agree to abide by the terms of the License. The License
  limits your use, and you acknowledge, that the Software may not be modified,
  copied or distributed unless embedded on a Texas Instruments microcontroller
  or used solely and exclusively in conjunction with a Texas Instruments radio
  frequency transceiver, which is integrated into your product.  Other than for
  the foregoing purpose, you may not use, reproduce, copy, prepare derivative
  works of, modify, distribute, perform, display or sell this Software and/or
  its documentation for any purpose.

  YOU FURTHER ACKNOWLEDGE AND AGREE THAT THE SOFTWARE AND DOCUMENTATION ARE
  PROVIDED �AS IS� WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED,
  INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF MERCHANTABILITY, TITLE,
  NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE. IN NO EVENT SHALL
  TEXAS INSTRUMENTS OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER CONTRACT,
  NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR OTHER
  LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
  INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE
  OR CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT
  OF SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
  (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

  Should you have any questions regarding your right to use this Software,
  contact Texas Instruments Incorporated at www.TI.com.
*******************************************************************************/

/*********************************************************************
 * INCLUDES
 */
#include "gatt.h"
#include "gattservapp.h"
#include "noiseBusterSensor.h"
#include "light_service.h"
#include "noise_service.h"
#include "board.h"

#include "string.h"
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Task.h>

// Edgar
#include <ti/drivers/lcd/LCDDogm1286.h>
#include "driverlib/adi.h"
#include "driverlib/aux_adc.h"
#include "driverlib/aux_wuc.h"
#include "board_lcd.h"
#include "board_key.h"
#include "Board.h"

// Edgar: Can't use _itoa from OnBoard.h use the one from LCDDogm1286_util.c
extern void _itoa(unsigned int uiNum, unsigned char *buf, unsigned char uiRadix);


/*********************************************************************
 * MACROS
 */

/*********************************************************************
 * CONSTANTS
 */

// How often to perform sensor reads (milliseconds)
#define SENSOR_DEFAULT_PERIOD   500

// Delay from sensor enable to reading measurememt
// (allow for 250 ms conversion time)
// Edgar: ADCs are fast, ~ 10us, therefor 0 ms
#define TEMP_MEAS_DELAY         0

// Length of the data for this sensor
#define SENSOR_DATA_LEN         LIGHT_SERVICE_DATA_LEN

// Event flag for this sensor
//#define SENSOR_EVT              LIGHT_SENSOR_EVT

// Task configuration
#define SENSOR_TASK_PRIORITY    1
#define SENSOR_TASK_STACK_SIZE  600

/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL VARIABLES
 */

/*********************************************************************
 * EXTERNAL FUNCTIONS
 */

/*********************************************************************
 * LOCAL VARIABLES
 */
// Entity ID used to check for source and/or destination of messages
static ICall_EntityID sensorSelfEntity;

// Semaphore used to post events to the application thread
static ICall_Semaphore sensorSem;

// Task setup
static Task_Struct sensorTask;
static Char sensorTaskStack[SENSOR_TASK_STACK_SIZE];

// Parameters
static uint8_t lightSensorConfig;
static uint8_t noiseSensorConfig;
static uint16_t sensorPeriod;
static uint16_t paramID;
static bool charChangePending;

// Edgar
static PIN_Config LightSensorTable[] = {
    IOID_26       | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW   | PIN_PUSHPULL | PIN_DRVSTR_MAX,     /* ALS_EN initially off          */
	/* IOID_23 is AL_OUT */
	PIN_TERMINATE
};

// Edgar
static PIN_Config NoiseSensorTable[] = {
    IOID_30       | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW   | PIN_PUSHPULL | PIN_DRVSTR_MAX,     /* NOISE_VCC initially low       */
	IOID_29       | PIN_GPIO_OUTPUT_EN | PIN_GPIO_LOW   | PIN_PUSHPULL | PIN_DRVSTR_MAX,     /* NOISE_GND low                 */
	/* IOID_28 is NOISE_OUT */
	PIN_TERMINATE
};

// Edgar
static uint16_t lightadc = 0;
static uint16_t noiseadc = 0;

/*********************************************************************
 * LOCAL FUNCTIONS
 */
static void sensorTaskFxn(UArg a0, UArg a1);
static void sensorConfigLightChangeCB(uint8_t paramID);
static void sensorConfigNoiseChangeCB(uint8_t paramID);
static void initLightCharacteristicValue(uint8_t paramID, uint8_t value,
                                    uint8_t paramLen);
static void initNoiseCharacteristicValue(uint8_t paramID, uint8_t value,
                                    uint8_t paramLen);

/*********************************************************************
 * PROFILE CALLBACKS
 */
static sensorCBs_t sensorLightCallbacks =
{
  sensorConfigLightChangeCB,  // Characteristic value change callback
};

static sensorCBs_t sensorNoiseCallbacks =
{
  sensorConfigNoiseChangeCB,  // Characteristic value change callback
};


/*********************************************************************
 * PUBLIC FUNCTIONS
 */

/*********************************************************************
 * @fn      AmbientLight_createTask
 *
 * @brief   Task creation function for the SensorTag
 *
 * @param   none
 *
 * @return  none
 */
void SensorTask_createTask(void)
{
  Task_Params taskParames;

  // Create the task for the state machine
  Task_Params_init(&taskParames);
  taskParames.stack = sensorTaskStack;
  taskParames.stackSize = SENSOR_TASK_STACK_SIZE;
  taskParames.priority = SENSOR_TASK_PRIORITY;

  Task_construct(&sensorTask, sensorTaskFxn, &taskParames, NULL);
}

/*********************************************************************
 * @fn      AmbientLight_ProcessCharChangeEvt
 *
 * @brief   SensorTag IR temperature event handling
 *
 */
void SensorTask_ProcessCharChangeEvt(uint8_t serviceID)
{
  if (charChangePending)
  {
    uint8_t newValue;

    if (SERVICE_ID_LIGHT == serviceID)
    {
    	switch (paramID)
		{
		case SENSOR_CONF:
		  if (lightSensorConfig != ST_CFG_ERROR)
		  {
			LightService_GetParameter(SENSOR_CONF, &newValue);

			if (newValue == ST_CFG_SENSOR_DISABLE)
			{
			  // Reset characteristics
			  initLightCharacteristicValue(SENSOR_DATA, 0, SENSOR_DATA_LEN);
			}

			lightSensorConfig = newValue;
		  }
		  else
		  {
			// Make sure the previous characteristics value is restored
			initLightCharacteristicValue(SENSOR_CONF, lightSensorConfig, sizeof ( uint8_t ));
		  }
		  break;
	/*
		case SENSOR_PERI:
		  IRTemp_getParameter(SENSOR_PERI, &newValue);
		  sensorPeriod = newValue * SENSOR_PERIOD_RESOLUTION;
		  break;
	*/
		default:
		  // Should not get here
		  break;
		}
    }

    if (SERVICE_ID_NOISE == serviceID)
    {
    	switch (paramID)
		{
		case SENSOR_CONF:
		  if (noiseSensorConfig != ST_CFG_ERROR)
		  {
			NoiseService_GetParameter(SENSOR_CONF, &newValue);

			if (newValue == ST_CFG_SENSOR_DISABLE)
			{
			  // Reset characteristics
			  initNoiseCharacteristicValue(SENSOR_DATA, 0, SENSOR_DATA_LEN);
			}

			noiseSensorConfig = newValue;
		  }
		  else
		  {
			// Make sure the previous characteristics value is restored
			initNoiseCharacteristicValue(SENSOR_CONF, noiseSensorConfig, sizeof ( uint8_t ));
		  }
		  break;
	/*
		case SENSOR_PERI:
		  IRTemp_getParameter(SENSOR_PERI, &newValue);
		  sensorPeriod = newValue * SENSOR_PERIOD_RESOLUTION;
		  break;
	*/
		default:
		  // Should not get here
		  break;
		}
    }


    charChangePending = false;
  }
}

/*********************************************************************
 * @fn      AmbientLight_reset
 *
 * @brief   Reset characteristics
 *
 * @param   none
 *
 * @return  none
 */
void SensorTask_reset(void)
{
  lightSensorConfig = ST_CFG_SENSOR_ENABLE;
  noiseSensorConfig = ST_CFG_SENSOR_ENABLE;
  initLightCharacteristicValue(SENSOR_DATA, 0, SENSOR_DATA_LEN);
  initLightCharacteristicValue(SENSOR_CONF, lightSensorConfig, sizeof(uint8_t));
  initNoiseCharacteristicValue(SENSOR_DATA, 0, SENSOR_DATA_LEN);
  initNoiseCharacteristicValue(SENSOR_CONF, lightSensorConfig, sizeof(uint8_t));
}


/*********************************************************************
* Private functions
*/

/*********************************************************************
 * @fn      sensorTaskInit
 *
 * @brief   Initialization function for the SensorTag IR temperature sensor
 *
 */
static void sensorTaskInit(void)
{

  // Edgar: Initialize ADC (Lightsensor)
  PIN_State lightsensor;
  PIN_open(&lightsensor, LightSensorTable);
  PIN_setOutputValue(&lightsensor, IOID_26, 1);

  ADIConfigSet(AUX_ADI4_BASE, ADI_SPEED_8 | ADI_WAIT_FOR_ACK, false);
  ADISync(AUX_ADI4_BASE);

  AUXWUCPowerCtrl(AUX_WUC_POWER_ACTIVE);
  AUXWUCClockEnable(AUX_WUC_ADI_CLOCK | AUX_WUC_SOC_CLOCK | AUX_WUC_SMPH_CLOCK | AUX_WUC_ADC_CLOCK);
  while (AUX_WUC_CLOCK_READY != AUXWUCClockStatus(AUX_WUC_ADC_CLOCK));

  IOCPortConfigureSet(IOID_23, IOC_PORT_AUX_IO, IOC_NO_IOPULL /* defaults otherwise 0 */ );
  AUXADCFlushFifo();
  AUXADCSelectInput(ADC_COMPB_IN_AUXIO7); // IOID_23
  AUXADCEnableSync(AUXADC_REF_FIXED, AUXADC_SAMPLE_TIME_85P3_US, AUXADC_TRIGGER_MANUAL);

  // Edgar: Initialize ADC (Noisesensor)
  PIN_State noisesensor;
  PIN_open(&noisesensor, NoiseSensorTable);
  PIN_setOutputValue(&noisesensor, IOID_29, 0);
  PIN_setOutputValue(&noisesensor, IOID_30, 1);

  IOCPortConfigureSet(IOID_28, IOC_PORT_AUX_IO, IOC_NO_IOPULL /* defaults otherwise 0 */ );
  // AUXADCSelectInput(ADC_COMPB_IN_AUXIO2); // IOID_28


  // Add service
  LightService_AddService();
  NoiseService_AddService();

  // Register callbacks with profile
  LightService_RegisterAppCBs(&sensorLightCallbacks);
  NoiseService_RegisterAppCBs(&sensorNoiseCallbacks);

  // Initilialize the module state variables
  sensorPeriod = SENSOR_DEFAULT_PERIOD;
  paramID = 0;
  charChangePending = false;

  // Initialise characteristics and sensor driver
  SensorTask_reset();
/*  initCharacteristicValue(SENSOR_PERI,
                          SENSOR_DEFAULT_PERIOD / SENSOR_PERIOD_RESOLUTION,
                          sizeof ( uint8_t ));*/
}

/*********************************************************************
 * @fn      sensorTaskFxn
 *
 * @brief   The task loop of the tempertaure readout task
 *
 * @return  none
 */
static void sensorTaskFxn(UArg a0, UArg a1)
{
  typedef union
  {
    struct
    {
      uint16_t tempTarget, tempLocal;
    } v;
    uint16_t a[2];
  } Data_t;

  // Register task with BLE stack
  ICall_registerApp(&sensorSelfEntity, &sensorSem);

  // Initialize the task
  sensorTaskInit();

  // Task loop
  while (true)
  {

    if (lightSensorConfig == ST_CFG_SENSOR_ENABLE)
    {

      // Edgar: Display buffer
      unsigned char buf[10];

      // Edgar: Read Lightsensor ADC and print to display
      AUXADCSelectInput(ADC_COMPB_IN_AUXIO7); // IOID_23
      AUXADCGenManualTrigger();
      lightadc = (uint16_t) AUXADCReadFifo();
      memset(buf, 0, sizeof buf);
      memcpy(buf, "ALS: ", 5);
      _itoa(lightadc, &buf[5], 10);
      LCD_WRITE_STRING((char *)buf, LCD_PAGE6);

      // Edgar: Read Noisesensor ADC and print to display
      AUXADCSelectInput(ADC_COMPB_IN_AUXIO2); // IOID_28
      AUXADCGenManualTrigger();
      noiseadc = (uint16_t) AUXADCReadFifo();
      memset(buf, 0, sizeof buf);
      memcpy(buf, "ANS: ", 5);
      _itoa(noiseadc, &buf[5], 10);
      LCD_WRITE_STRING((char *)buf, LCD_PAGE7);

      LightService_SetParameter(SENSOR_DATA, SENSOR_DATA_LEN, &lightadc);
      NoiseService_SetParameter(SENSOR_DATA, SENSOR_DATA_LEN, &noiseadc);

      // Next cycle
      delay_ms(sensorPeriod - TEMP_MEAS_DELAY);
    }
    else
    {
      delay_ms(SENSOR_DEFAULT_PERIOD);
    }
  }
}


/*********************************************************************
 * @fn      sensorChangeCB
 *
 * @brief   Callback from IR Temperature Service indicating a value change
 *
 * @param   paramID - parameter ID of the value that was changed.
 *
 * @return  none
 */
static void sensorConfigLightChangeCB(uint8_t paramIDChanged)
{
  paramID = paramIDChanged;
  charChangePending = true;

  // Wake up the application thread
  NoiseBusterSensor_charValueChangeCB(SERVICE_ID_LIGHT);
}

static void sensorConfigNoiseChangeCB(uint8_t paramIDChanged)
{
  paramID = paramIDChanged;
  charChangePending = true;

  // Wake up the application thread
  NoiseBusterSensor_charValueChangeCB(SERVICE_ID_NOISE);
}


/*********************************************************************
 * @fn      initCharacteristicValue
 *
 * @brief   Initialize a characteristic value
 *
 * @param   paramID - parameter ID of the value is to be cleared
 *
 * @param   value - value to initialise with
 *
 * @param   paramLen - length of the parameter
 *
 * @return  none
 */
static void initLightCharacteristicValue(uint8_t paramID, uint8_t value,
                                    uint8_t paramLen)
{
  uint8_t data[SENSOR_DATA_LEN];

  memset(data,value,paramLen);
  LightService_SetParameter( paramID, paramLen, data);
}

static void initNoiseCharacteristicValue(uint8_t paramID, uint8_t value,
                                    uint8_t paramLen)
{
  uint8_t data[SENSOR_DATA_LEN];

  memset(data,value,paramLen);
  NoiseService_SetParameter( paramID, paramLen, data);
}

/*********************************************************************
*********************************************************************/
